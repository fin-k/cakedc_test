<?php
namespace App\Model\Table;

use CakeDC\Users\Model\Table\UsersTable;

/**
 * Users Model
 */
class ClientsTable extends UsersTable
{

	 public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clients');
    }
}

?>